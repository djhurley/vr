﻿using UnityEngine;
using System.Collections;

public class Drawer : MonoBehaviour {

    public Material lineMaterial;
    private bool isDrawing = false;
    private LineRenderer line;

    void Start ()
    {
        line = gameObject.AddComponent<LineRenderer>();
        line.material = lineMaterial;
        line.SetWidth(0.2f, 0.2f);
        line.useWorldSpace = true;   

        line.SetVertexCount(5);
        line.SetPosition(0, new Vector3(0, 0, 0));
        line.SetPosition(1, new Vector3(0, 1, 0));
        line.SetPosition(2, new Vector3(1, 1, 0));
        line.SetPosition(3, new Vector3(1, 0, 0));
        line.SetPosition(4, new Vector3(0, 0, 0));
    }
	
	void Update () {
        
	}

    public void setIsDrawing(bool isDrawing)
    {
        this.isDrawing = isDrawing;
    }

}
