﻿using UnityEngine;
using System.Collections;

public class TargetHitDetection : MonoBehaviour {

    public bool magnetDetectionEnabled = true;
    private Animator animator;
    private bool isLookedAt = false;

    void Start()
    {
        animator = GetComponent<Animator>();
        CardboardMagnetSensor.SetEnabled(magnetDetectionEnabled);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    } 

    void Update()
    {
        if (magnetDetectionEnabled)
        {
            if (CardboardMagnetSensor.CheckIfWasClicked())
            {
                CardboardMagnetSensor.ResetClick(); 

                if(isLookedAt){
                    animator.SetBool("targetHit", true);
                }

            }
            else
            {
                animator.SetBool("targetHit", false);
            }
        }
	}

    public void SetIsLookedAt(bool isLookedAt)
    {
        this.isLookedAt = isLookedAt;
    }
}
